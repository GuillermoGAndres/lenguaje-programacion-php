# Lenguaje de programción PHP

* Andres Urbano Guillermo Gerardo

### Running apache server via Linux command-line.
~~~
sudo /opt/lampp/lampp start
~~~
### Running apache server via Linux GUI
~~~
cd /opt/lampp
sudo ./manager-linux.run (or manager-linux-x64.run)
~~~
### Stop apache server
~~~
sudo /opt/lampp/lampp stop
or via GUI:
cd /opt/lampp
sudo ./manager-linux.run (or manager-linux-x64.run)
~~~~


