<?php 
// esta etiqueta siempre debe de ir para activar la sesion
session_start();
?>
<html>
	<head>
		<title> Sesiones </title>
	</head>
	<body>
	<?php 
	// las sesiones se pueden ocupan en cualqueir lado despues del session_start();
	$_SESSION['user'] = 'hgcm1';
	$_SESSION['nombre'] = 'Hugo';
	$_SESSION['pasatiempos'] = ['Futbol', 'Videojuegos', 'Tecnología'];
	?>
		<p>Se ha creado la sesion</p>
		<a href='./leer.php'>Leer Sesion</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href='./borrar.php'>Borrar Sesion</a>
	</body>
</html>