<?php 
// forma de declarar arreglos
$arreglo = array(1,2,3,4);
$arreglo1 = [1,2,3,4];
//se les puede definir un indice
$arreglo2 = [1=>1, 2=>2, 3=>4, 4=>3];
// su indice no necesariamente debe ser numerico
$arreglo3 = ['num'=>'400', 'nombre'=>'Hugo', 'apellido1' => 'Cuellar'];
//se pueden crear arreglos multidimensionales
$arreglo4 = [
	'usuario' => $arreglo3,
	'pasatiempos' => ['Videojuegos','Tecnologia', 'Deportes']
];
?>
<pre>
<?php
print_r($arreglo);
print_r($arreglo1);
print_r($arreglo2);
print_r($arreglo3);
print_r($arreglo4);
?>
</pre>
<p>recorrer arreglos</p>
<pre>
<?php 
//esto funciona bien con indices numericos y seguidos pero que apsaria con arreglo2
for ($x=0; $x < count($arreglo); $x++){
	echo $arreglo[$x];
}
echo "<br />";
foreach($arreglo3 as $key => $val) {
	echo "{$key} ===> {$val}<br/>";
}
// se pueden borrar datos del arreglo 
unset($arreglo[0]);
unset($arreglo2[3]);
unset($arreglo3['num']);

// o borrar todo el arreglo 
unset($arreglo1);

//agregar datos a un arreglo 
$arreglo4['usuario']['login'] = 'HGCM0456';
// recuerden que es sensible a mayusculas y minusculas
$arreglo4['Usuario']['login'] = 'HGCM0456';

?>
</pre>
<pre>
<?php
print_r($arreglo);
print_r($arreglo1);
print_r($arreglo2);
print_r($arreglo3);
print_r($arreglo4);
?>
</pre>
