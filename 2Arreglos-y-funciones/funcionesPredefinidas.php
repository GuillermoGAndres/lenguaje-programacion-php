<?php
// substr - Devuelve parte de una cadena
echo 'Method substr <br>';
$word = "HolaMundo";
echo substr($word, 0, 4),"<br>";
echo substr("holamundo",4), '<br>';
//strstr - Encuentra la primera aparición de un string
echo 'Method strstr<br>';
$email = "gerardobec35@gmail.com";
$domain = strstr($email, '@');
echo $domain, '<br>';
$name = strstr($email, '@', true);
echo $name, '<br>';
//
echo 'strpos -  Encuentra la posición de la primera ocurrencia de un substring en un string <br>';
$word = "Anita lava la tina";
echo $word,'<br>';
echo 'Pos lava: ', strpos($word, "lava"), '<br>';
//implode - Join array elements with a string
$alumno = ["Andres", "22años", "Computacion"];
$infoAlum = implode("# ",$alumno);
echo $infoAlum, '<br>';
//explode - Split a string by array substrin
$path = "/home/guillermo/Documents/dgtic";
$directories = explode("/", $path);
echo $directories[0], "<br>", $directories[1], "<br>" ,$directories[2], "<br>";
$information = "This is message secret";
$binaries = utf8_decode($information);
echo $binaries, '<br>';
$information = utf8_encode($binaries);
echo $information, '<br>';
$arrayFruits = ["banana", "cereza", "limon", "piña"];
print_r($arrayFruits);
echo "<br>";
$lastFruit = array_pop($arrayFruits);
print_r($arrayFruits);
echo "Last fruit: $lastFruit";
?>
