<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <link rel="stylesheet" href="formulario.css">
    <link rel="stylesheet" href="navbar.css">
    <style>
        body {
            margin-top:0px;
        }
        .container {
            margin: 0px 150px;
            padding: 0px;
            background-color: white;
        }
        nav {
            font-family: Impact, Charcoal, sans-serif;
            background-color: lightseagreen;
        }
        nav a:hover{
                background-color: lightyellow;
        }
        input {
            width: 35%;
            display: block;
            font-size: 20px;
        }
        input[type=radio] {
            width: 50px;
            display: inline;
            margin: 17px 0px;
        }
        button[type=submit] {
            width: 10%;
        }
    </style>
</head>
<body>
    
    <?php
        session_start();
        //Con esto registringe al poner la direccion url sin estar registrado
        if( !$_SESSION['login'] ) {   
            header('Location: login.php');
            exit;
        }
    ?>
    <div class="container">
        <nav>
            <ul>
                <li><a href="info.php"> Home </a></li>
                <li><a href="formulario.php"> Registrar Alumnos </a></li>
                <li><a href="cerrar.php">Cerrar sesión </a></li>
            </ul>
        </nav>
        <div>
            <form action="info.php" method="POST">
                <div>
                    <label for="num_cta"> Numero de Cuenta: </label>
                    <input type="text" id="num_cta" name="num_cta">
                </div>
                <div>
                    <label for="nombre">Nombre: </label>
                    <input type="text" id="nombre" name="nombre">
                </div>
                <div>
                    <label for="primer_apellido"> Apellido Paterno: </label>
                    <input type="text" id="primer_apellido" name="primer_apellido">
                </div>
                <div>
                    <label for="segundo_apellido"> Apellido Materno: </label>
                    <input type="text" id="segundo_apellido" name="segundo_apellido">
                </div>
        
                <div>
                    <label>Género</label>
                    <ul>
                        <li>
                            <input type="radio" id="hombre" name="genero" value="hombre" >
                            <label for="">Hombre</label>
                        </li>
                        <li>
                            <input type="radio" id="mujer" name="genero" value="mujer">
                            <label for="mujer"> Mujer </label>
                        </li>
                        <li>
                            <input type="radio" id="otro" name="genero" value="otro">
                            <label for="otro"> Otro </label>
                        </li>
                    </ul>
                </div>
        
                <div>
                    <label for="fecha_nac"> Fecha de Nacimiento: </label>
                    <input type="date" id="fecha_nac" name="fecha_nac">
                </div>
        
                <div>
                    <label for="contrasena">Constraseña: </label>
                    <input type="password" id="contrasena" name="contrasena">
                </div>
        
                <button type="submit">Enviar </button>
            </form>
        </div>   
    </div>
</body>
</html>
