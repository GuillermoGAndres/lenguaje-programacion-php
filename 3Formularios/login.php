<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="formulario.css">
    <style>
        fieldset {
            border: 8px solid groove;
            border-radius: 40px;
            position: relative;
            padding: 20px;
        }

        legend {
        font-family: Verdana, Geneva, sans-serif;
        font-size: 30px;
        position: absolute;
        top: 0;
        right: 50%;
        }
        #container {
            margin: 150px 300px;
            border-radius: 20px
        }
        button[type=submit] {
            margin-left: 270px;
        }
    </style>
</head>
<body>

    <?php 
    session_start();
    $_SESSION['alumnoRegistrado'] = "";
    $_SESSION['login'] = false; // Nos indica si ya esta logeado
    
    /**
     * Verifica si encuentra registrado la persona a ingresar y guarda sus valores en alumnoRegistrado
     */
    function estaRegistrado($num_cta, $contrasena) {
        foreach ($_SESSION['alumnos'] as $key => $alumno) {
            if ( $alumno['num_cta'] === $num_cta && $alumno['contrasena'] === $contrasena ) {
                //$GLOBALS['alumno'] = $alumno; 
                $_SESSION['alumnoRegistrado'] = $alumno;
                return true;
            }
            
        }
        return false;
    }


    function loggear() {
        $_SESSION['login'] = true;
        header('Location: info.php');
        exit;
    }

    function iniciarSesion() {
        if (estaRegistrado($_POST["num_cta"],$_POST["contrasena"])) {
            loggear();
        }
        echo "<center><div style=\"font-family: Impact, Charcoal, sans-serif;font-size:30px; border: 1px solid black; background: lightyellow; display:inline;\">  Usuario y/o contraseña Invalida.   </div> </center>";
    }

    if (!isset( $_SESSION['alumnos'])) {
        echo "Esta vacion SESSION, crear SESSION".'<br>';
        $_SESSION['alumnos'] = [
            1 => [
                'num_cta' => '1',
                'nombre' => 'Admin',
                'primer_apellido' => 'General',
                'segundo_apellido' => "",
                'contrasena' => 'adminpass123',
                'genero' => 'O',
                'fecha_nac' => '25/01/1990'
            ],
        ];
    }

    //print_r($_POST);
    //print_r($_SESSION['alumnos']);
     /**
     * $_POST - estara vacio la primera vez, cuando presione ingresar, $_POST ya tendra valores para iniciar sesion. 
     */
    if(!empty($_POST)) {
        iniciarSesion();
    }
        
    
    ?>

    <div id="container">
        <form action="login.php" method="POST">
            <fieldset>
                <legend>Login</legend> 
                
                <div>
                    <label for="num_cta">Numero de cuenta <abbr title="required">*</abbr> : </label>
                    <input type="text" id="num_cta" name="num_cta" placeholder="Escriba su numero de cuenta">
                </div>
                <div> 
                    <label for="contrasena">Contraseña <abbr title="required">*</abbr> :</label> 
                    <input type="password" id="contrasena" name="contrasena" placeholder="Escriba su contraseña"> 
                </div>
                <button type="submit"> Ingresar </button>
            </fieldset>
        </form>
    </div>
    
</body>
</html>
