<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Info</title>
    <link rel="stylesheet" href="navbar.css"> 
    <link rel="stylesheet" href="info.css"> 
    <style>
        body {
            margin-top:0px;
        }
        #container {
            font-family: Impact, Charcoal, sans-serif;
        }
        thead {
            background-color: lightseagreen;
        }   

        table {
        /*border: 1px solid black;*/
            font-size: 20px;
            width: 1002px;
        }
        nav {
            background-color: lightseagreen;
            
        }
    
        nav a:hover{
            background-color: lightyellow;
        }
        table {
            border: 0px
        }
        td {
            border: 0px;
            border-top: 1px solid black;
        }
       

    </style>
</head>
<body>
    
    <?php
    /*En $_SESSION['alumnos'] - tendre a todos los perfiles registrados 
    * Validar usuario, buscara en SESSION si el nombre y contraseña coinciden con un
    * alumno.
     */
    session_start();
    $alumno = ""; //variable Global que sera el alumno encontrado
   
    function registrarAlumno($num_cta, $nombre, $primer_apellido, $segundo_apellido, $genero, $fecha_nac, $contrasena) {
        $alumno = [
            'num_cta' => $num_cta,
            'nombre' => $nombre,
            'primer_apellido' => $primer_apellido,
            'segundo_apellido' => $segundo_apellido,
            'contrasena' => $contrasena,
            'genero' => $genero,
            'fecha_nac' => $fecha_nac 
        ]; 
        array_push($_SESSION['alumnos'], $alumno);
    }

    //Con esto registringe al poner la direccion url sin estar registrado
    if( !$_SESSION['login'] ) {   
        header('Location: login.php');
        exit;
    }
    $alumno = $_SESSION['alumnoRegistrado']; //Obtendo a la persona que logio.
    
    /**
     * Cuando llegue una peticion de forumulario.php, info.php es la que va recibir, por lo tanto es
     * el va registrar a los nuevos alumnoes
     */
    if(!empty($_POST)) {
       registrarAlumno($_POST['num_cta'], $_POST['nombre'], $_POST['primer_apellido'], $_POST['segundo_apellido'], $_POST['genero'],$_POST['fecha_nac'], $_POST['contrasena']);
       //print_r($_SESSION);
    }

    ?>

    <!--Aqui es donde se debe conectar cuanda haga login, en tabbar debe tener
        la informacion del usuario logiado, formulario para agregagar a un nuevo usuario
        y botono salir, cada pestaña enzala a su correspondiente pagina.
    -->
    <div id="container">
        <nav>
            <ul>
                <li><a href="info.php"> Home </a></li>
                <li><a href="formulario.php"> Registrar Alumnos </a></li>
                <li><a href="cerrar.php">Cerrar sesión </a></li>
            </ul>
        </nav>

        <main>

            <section id="InfoUser">
                <h2>Usuario Identificado</h2>
                <div id="informacion">
                    <p id="nombreUsuario"> <?php echo $alumno['nombre']," ", $alumno['primer_apellido'];?> </p>
                    <h3 id="headerInfo"> Información </h3>
                    <ul class="ListaInformacion">
                        <li>Numero de cuenta: <?php echo $alumno['num_cta']?></li>
                        <li>Fecha de nacimiento: <?php echo $alumno['fecha_nac'] ?> </li>
                    </ul>
                </div>  
            </section>

            <section id="Datos Guardados">
                <h2>Datos Guardados:</h2>
                <table>
                    <thead>
                        <tr> 
                            <th>#</th>
                            <th>Nombre </th>
                            <th> Fecha de nacimiento</th>
                        </tr>
                    </thead>
                    <tbody>
                     <!--    <tr>
                            <td>1</td>
                            <td>Guillermo</td>
                            <td>25/06/1998</td>
                        </tr> --> 
                        <!-- Agregar dinamicamentes con php -->
                    <?php 
                        foreach($_SESSION['alumnos'] as $numAlumno => $alumno) {
                            echo "<tr>";
                            echo "<td>", $alumno['num_cta'], "</td>";
                            echo "<td>", $alumno['nombre'], "</td>";
                            echo "<td>", $alumno['fecha_nac'], "</td>";
                            echo "</tr>";
                        }
                    ?>
                    </tbody>
                </table>
            </section>          
        </main>
    </div>
    
</body>
</html>
