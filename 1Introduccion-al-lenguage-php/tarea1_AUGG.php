<?php
// Andres Urbano Guillermo Gerardo.

function generarPiramide($MAX_ROW) {
    $asterisk = "* ";
    for ($i = 1; $i <= $MAX_ROW; $i++) {
        echo "<center> $asterisk </center>";
        $asterisk .= "* ";
    }
} 

function generarRombo($MAX_ROW) {
    generarPiramide($MAX_ROW);
    for ($i = $MAX_ROW -1; $i >= 0 ; $i--) {
        $asterisk = "";
        for($j=0; $j < $i; $j++) {
            $asterisk .= "* ";
        }
        echo "<center> $asterisk </center>";
    }
}

$MAX_ROW = 30;
echo "<h1 style=\"font-size:50px;\"> <center> Piramide y Rombo </center> </h1>";
echo "<h2 style=\"font-size:40px;\"> <center> Piramide </center> </h2>";
generarPiramide($MAX_ROW);
echo "<h2 style=\"font-size:40px;\"> <center> Rombo </center> </h2>";
generarRombo($MAX_ROW);

?>
