<?php
//Realizar una expresión regular que detecte emails correctos.
echo "1.- Realizar una expresión regular que detecte emails correctos. <br>";
$correo = "Este es mi email memocampeon35@gmail.com como ejemplo para una expresion regular";
echo $correo . '<br>';
$coincidencias;
$numCoincidencias = preg_match('/\S+@\S+/',$correo, $coincidencias);
echo "Coincidencia: ",$numCoincidencias, '<br>';
echo "Correos: "; print_r($coincidencias); //var_dump($coincidencias);

//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
echo "<br> 2.- Realizar una expresion regular que detecte Curps Correctos <br>";
$curpEjemplo = "Es es un curp ABCD123456EFGHIJ78 de ejemplo ";
$numCoincidencias = preg_match('/[A-D]+[1-6]+[E-J]+[7-8]+/', $curpEjemplo, $coincidencias);
echo "Coincidencia: ",$numCoincidencias, '<br>';
echo "Curp: "; print_r($coincidencias); //var_dump($coincidencias);


//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
echo "<br>3.- Realizar una expresion regular que detecte palabras de longitud mayor a 50 <br> ";
$palabra = "Esta es una cadena de de mas 50 caractes de largo : Caracteresaaaaaaaaaaaakkkkkkkkkasdfjkashflashlsdajhkljhasjfdasjkladfaasdfasddfadassdfdasdfsdfsdh y otroa mas";
echo $palabra, '<br>';
$numCoincidencias = preg_match('/ [a-zA-Z\S]{50,}/', $palabra, $coincidencias);
echo "Coincidencia: ",$numCoincidencias, '<br>';
echo "Palabra: "; print_r($coincidencias); //var_dump($coincidencias);


//Crea una funcion para escapar los simbolos especiales.
echo "<br> 4.- Crea una funcion para escapar los simbolos especiales.<br>";
function escaparSimbolosEspeciales($cadena) {
    for($i=0; $i < strlen($cadena); $i++) {
    $patron = "/[^a-zA-Z0-9\s]+/";  // Coincidencia de uno o mas caracteres que no sean letraMayusculas,minisculas, numeros y espacios, por lo tanto sera un caracter especial.
    $sustituir = "\\".$cadena[$i];
    echo preg_replace($patron, $sustituir, $cadena[$i]);
    } 
}
$cadenaConSimbolosEspeciales = "Esta es una cadena con simbolos especiales & este es otro < y otro > con acento % # ! y otro mas * ~ y este mas ^";
echo "Cadena con simboloes especiales antea de entrar a ala funcion de escapar <br>", $cadenaConSimbolosEspeciales;
echo "<br> Cadena despues de entrar a la funcion<br>";
escaparSimbolosEspeciales($cadenaConSimbolosEspeciales);

//Crear una expresion regular para detectar números decimales.
echo "<br> 5.- Crear una expresion regular para detectar números decimales. <br> ";
//[0-9.]+
$numeroDecimal = "Este es un numero decimal 345.345343 y este es otro 3.141654634 ";
echo $numeroDecimal,"<br>";
$numCoincidencias = preg_match_all('/[0-9.]+/', $numeroDecimal, $coincidencias);
echo "Coincidencia: ",$numCoincidencias, '<br>';
echo "Numeros decimales: "; print_r($coincidencias); //var_dump($coincidencias);