<?php 
/* URL para ver los estrenos de películas
* https://api.themoviedb.org/3/movie/upcoming?api_key=[KEY]&language=[LANG]&page=[PAGE]
*/
$url_base ='https://api.themoviedb.org/3';
$url_img = 'https://image.tmdb.org/t/p/w220_and_h330_face';
$api = 'api_key=';
$language = 'es-Mx';
$page = 1;
if (isset($_REQUEST['page'])) {
    $page = $_REQUEST['page'];
}
$res = null;

try {
    $ch = curl_init();
    $url = "{$url_base}/movie/upcoming?{$api}&language={$language}&page={$page}";
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // texto
    $res = curl_exec($ch);
    // array
    $res = json_decode($res);
} catch (\Exception $e) {
    var_dump($e);
} finally {
    curl_close($ch);
}

/** 
 * @todo Buscar películas
 * url https://api.themoviedb.org/3/search/movie?api_key=[KEY]&language=[LANG]&query=[TEXTO A BUSCAR]&page=[PAGE]
*/
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Consumir APis con PHP y curl</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="./estilos.css" />
</head>
<body>
    <div class="container-fluid">
        <h1>Pel&iacute;culas</h1>
        <div class="row">
            <?php foreach($res->results as $pel): ?>
            <div class="col-md-6 col-lg-3">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top mx-auto d-block" src="<?= $url_img . $pel->poster_path ?>" alt="<?=  $pel->title ?>">
                    <div class="card-body">
                        <h5 class="card-title"><?= $pel->title; ?></h5>
                        <p class="card-text"><?php
                            echo substr($pel->overview, 0, 200);
                            echo (strlen($pel->overview) > 200) ? '...' : '';
                        ?></p>
                        <a href="detalle.php?movie_id=<?= $pel->id ?>" class="btn btn-outline-primary btn-block">Detalle</a>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="row">
            <div class="col-md-3">
                Total Películas: <?= $res->total_results ?>
                Total Paginas: <?= $res->total_pages ?>

            </div>
            <div class="col-md-9">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Anterior</span>
                        </a>
                        </li>
                        <?php for ($x=1; $x <= $res->total_pages; $x++) { ?>
                            <li class='page-item <?= ($res->page == $x ) ? "disabled" : "" ?>'><a class='page-link' href="<?= "index.php?page={$x}" ?>"><?= $x ?></a></li>
                        <?php } ?>
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Siguiente</span>
                        </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>