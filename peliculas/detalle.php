<?php
if (!isset($_REQUEST['movie_id']) && !is_numeric($_REQUEST['movie_id'])) {
    header('Location: index.php');
    exit;
}
// https://api.themoviedb.org/3/movie/[MOVIE_ID]?api_key=[KEY]&language=[LANG]
$url_base ='https://api.themoviedb.org/3/movie';
$url_img = 'https://image.tmdb.org/t/p/w220_and_h330_face';
$api = 'api_key=';
$movie_id = $_REQUEST['movie_id'];
$language = 'es-Mx';
$movie = null;
try {
    $mensaje = null;
    $ch = curl_init();
    $url = "{$url_base}/{$movie_id}?{$api}&language={$language}";
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $movie = curl_exec($ch);
    $movie = json_decode($movie);    
    var_dump($movie);
    if (isset($movie->success) && $movie->success == false) {
        $mensaje = $movie->status_message;
        $movie = null;
    }
} catch (\Exception $e) {
    var_dump($e);
} finally {
    curl_close($ch);
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Detalle de la película</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="./estilos.css" />
</head>
<body>
    <div class="container">
        <?php if (!empty($movie)): ?>
            <h1><?= $movie->title ?></h1>
            <div class="row">
                <div class="col-md-4">
                    <img src="<?= "$url_img/{$movie->poster_path}"; ?>" class="img-fluid" alt="...">
                </div>
                <div class="col-md-8">
                    <p>Titulo Original: <strong><?= $movie->original_title ?></strong></p>
                    <p><?= $movie->tagline; ?></p>
                    <p> <strong>Géneros:</strong> <br />
                        <ul> 
                            <?php foreach($movie->genres as $genre) {
                                echo "<li>{$genre->name}</li>";

                            } ?>
                        </ul>                   
                    </p>
                    <p><?= $movie->overview; ?></p>
                    <p><strong>Compañias:</strong> <br />
                            <?php foreach($movie->production_companies as $company) {
                                if (!empty($company->logo_path)) {
                                    echo "<img src='{$url_img}{$company->logo_path}' width='10%' class='img-fluid img-thumbnail' alt='{$company->name}' title='{$company->name}'>";
                                }
                            } ?>
                    </p>
                    <p><a href="<?= $movie->homepage ?>" class="btn btn-info">Página web</a></p>            
                </div>
            </div>
        <?php else: ?>
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-warning" role="alert"><?= $mensaje ?></div>
                </div>
            </div>
        <?php endif; ?>
        <a href="index.php" class="btn btn-primary">Regresar</a>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>